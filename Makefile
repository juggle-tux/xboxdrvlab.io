all:
	mkdir -p build
	cp -v *.css *.html *.png *.jpg *.svg *.tar.bz2 build

manpage:
	cp -v ../xboxdrv/doc/xboxdrv.html/index.html xboxdrv.html

upload:
	./publish.sh

# EOF #
